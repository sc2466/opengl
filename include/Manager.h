#ifndef MANAGER_H
#define MANAGER_H

#include "myHeaders.h"

template<class T>
class Manager
{
public:
    ~Manager()
    {
        removeAll();
    }

    // Make Create Function later
    void create()
    {
        add(new T());
    }

    // Remove with Index
    void remove(const uint index)
    {
        if(list_.size() > index)
            list_.erase(list_.begin() + index);
    }

    // Remove with Pointer
    void remove(const T* target)
    {
    }

    // Remove everything with Memory
    void removeAll()
    {
        Vector<T>().swap(list_);
#ifdef _DEBUG
        std::cout << list_.capacity() << std::endl;
#endif
    }

protected:

    inline void add(const T* obj)
    {
        list_.push_back(*obj);
    }

    Vector<T> list_;
};

#endif // MANAGER_H
