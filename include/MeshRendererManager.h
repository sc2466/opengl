#ifndef MESHRENDERERMANAGER_H
#define MESHRENDERERMANAGER_H

#include "Manager.h"
#include "MeshRenderer.h"


class MeshRendererManager : public Manager<MeshRenderer>
{
public:
    void create(Mesh* mesh, Shader* shader);

    void render(Camera* camera);
};

#endif // MESHRENDERERMANAGER_H
