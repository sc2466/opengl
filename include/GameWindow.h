#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include "Types.h"

class Scene;
class UICamera;
class InputSystem;
class GLFWwindow;

class GameWindow
{
    public:
        GameWindow(uint width = 800u, uint height = 500u);
        virtual ~GameWindow();


        // Init GLFW and Create window with my window size
        // And Init GLAD
        int create();

        void loop();

        void setCursorEnable(uint bIsEnable);

        static void mouse_callback(GLFWwindow* window, double xpos, double ypos);
        static void framebuffer_size_callback(GLFWwindow* window, int width, int height);

        //---------------------------------------------
        // get set

        inline GLFWwindow* getGLFWWindow() const  {return glfw_window_;}
        inline const Scene* getScene() const            {return scene_;}

        inline const uint getWidth() const              {return width_;}
        inline const uint getHeight() const             {return height_;}
        inline const uint getHalfWidth() const          {return halfWidth_;}
        inline const uint getHalfHeight() const         {return halfHeight_;}

        static inline GameWindow* get(){return self_;}

    protected:

        // Init GLFW
        static int GLFW_Init();
        // Init GLAD
        static int GLAD_Init();

        GLFWwindow* glfw_window_;
        Scene* scene_;

        uint width_, height_;
        uint halfWidth_, halfHeight_;

        String name_ = "MyOpenGL";

        friend InputSystem;
        friend UICamera;

    private:
        static GameWindow* self_;
};

#endif // GAMEWINDOW_H
