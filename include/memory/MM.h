#ifndef MM_H
#define MM_H

#include <memory>
#include "StackAllocator.h"
#include "LinearAllocator.h"
#include "FreeListAllocator.h"
#include "PoolAllocator.h"

#define MB 1048576 //1MB
#define MB20 MB*20 //20MB

template<class T>
class MM
{
public:
    static MM* create(size_t _size){
        MM* t = new MM(_size);
        t.allocMemory(_size);
    }

    // Allocate form Free list
    template<class T1> T1 alloc(){
        return allocator::allocateNew(pAllocator);
    }

    // Deallocate form Free list
    template<class T1> void dealloc(T1& object){
        allocator::deallocateDelete(pAllocator, object);
    }

protected:
    ~MM(){deallocMemory();}

private:
    void allocMemory(size_t _size){
        pMem_ = malloc(_size);
        pAllocator = new T(_size, pMem_);
    }

    void deallocMemory(){
        free(pMem_);
        delete pAllocator;
    }

    // Global saving data pointer
    void* pMem_ = nullptr;

    // Memory Allocator
    T* pAllocator;
};

#endif // MM_H
