#ifndef POINTLIGHT_H
#define POINTLIGHT_H

#include "Light.h"

class PointLight : public Light
{
public:
    PointLight(const Vec3& pos = Vec3(0), const Vec3& color = Vec3(1));
    virtual ~PointLight();

    // set uniform Light matrix to shader
    void setLightMatToShader(Shader* shader);

    // Render every meshs to shadow depth texture toward FBO
    void RenderShadowMap(const Vector<Mesh*>& mesh_list);

protected:

    // Create shadow map and FBO
    void CreateShadowMap();

    // use for make shadow
    uint shadow_width_ = 1024;
    uint shadow_height_ = 1024;
    uint depth_fbo_ = 0;
    DepthTexture* shadow_tex_ = nullptr;

private:
};

#endif // POINTLIGHT_H
