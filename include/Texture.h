#ifndef TEXTURE_H
#define TEXTURE_H

#include "myHeaders.h"
#include "stb_image.h"

class Texture
{
public:
    ~Texture();

    void LoadImage(const char* resourece, bool gammaCorrection = false);
    uint LoadCubemap(Vector<const char*> faces);

    operator uint() const {return textureID_;}
    uint get() const {return textureID_;}

protected:
    int width_, height_, nrComponents_;
    uint textureID_;
};

#endif // TEXTURE_H
