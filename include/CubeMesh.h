#ifndef CUBEMESH_H
#define CUBEMESH_H

#include "Mesh.h"


class CubeMesh : public Mesh
{
    public:
        virtual ~CubeMesh();
        void Render();

        static Mesh* get();

    protected:

    private:
        CubeMesh();
        static Mesh* self_;

        static void Create();

        static uint cubeVAO;
        static uint cubeVBO;
};

#endif // CUBEMESH_H
