#ifndef CAMERA_H
#define CAMERA_H

#include "myHeaders.h"

class GameWindow;

class Camera
{
public:

    // Constructor with vectors
    Camera(const Vec3& position = Vec3(0.0f, 0.0f, 10.0f), const Vec3& up = Vec3(0.0f, 1.0f, 0.0f), const float yaw = YAW, const float pitch = PITCH);
    // Constructor with scalar values
    Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch);

    void calculate();
/*
    // Processes input received from any keyboard-like input system. Accepts input parameter in the form of camera defined ENUM (to abstract it from windowing systems)
    void ProcessKeyboard(Camera_Movement direction, float deltaTime);

    // Processes input received from a mouse input system. Expects the offset value in both the x and y direction.
    void ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch = true);
*/
    // If change the window size then game window will call this function
    inline void setSize(uint width, uint height){
        width_=width; height_=height;
        windowRatio_ = (float)width_/height_;
        bIsNeedCalProj_ = true;
    }
    inline void setPos(const Vec3& pos){
        position_ = pos;
        bIsNeedCalView_ = true;
    }
    inline void setYaw(const float yaw){
        yaw_ = yaw;
        bIsNeedCalView_ = true;
    }
    inline void setPitch(const float pitch){
        pitch_ = pitch;
        bIsNeedCalView_ = true;
    }

    inline const Vec3& getPos() const {return position_;}
    inline const float getYaw() const {return yaw_;}
    inline const float getPitch() const {return pitch_;}
    inline const Vec3& getForward() const {return forward_;}
    inline const Vec3& getRight() const {return right_;}

    // Returns the view matrix calculated using Eular Angles and the LookAt Matrix
    inline const Mat4& getViewMatrix() {calculate();return view_mat_;}
    inline const Mat4& getProjectionMatrix() {calculate();return proj_mat_;}
    inline const Mat4& getVpMatrix() {calculate();return vpMat_;}


protected:
    // Default camera values
    static const float YAW;
    static const float PITCH;
    static const float ZOOM;
    static const float FOV;

    // Camera Attributes
    Vec3 position_;
    Vec3 up_, forward_, right_;
    Vec3 world_up_;

    // Eular Angles
    float yaw_;
    float pitch_;
    float roll_;

    // Camera options
    float zoom_;
    float fov_;

    uint width_, height_;

    float near_ = .1f, far_ = 100.f;

    Mat4 view_mat_;
    Mat4 proj_mat_;
    Mat4 vpMat_;
    // Calculates the front Vector from the Camera's (updated) Eular Angles

private:
    uint bIsNeedCalView_ = 1;
    uint bIsNeedCalProj_ = 1;

    float windowRatio_;
};
#endif
