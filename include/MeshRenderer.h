#ifndef MESHRENDERER_H
#define MESHRENDERER_H

#include "Camera.h"
#include "Mesh.h"
#include "Shader.h"
#include "Transform.h"

/**< Use for displace ForwardModel DeferredModel */
class MeshRenderer
{
    public:
        MeshRenderer(Mesh* mesh, Shader* shader);
        virtual ~MeshRenderer();

        void render(Camera* camera);

    protected:
        /**< Render meshes */
        Mesh* mesh_;

        /**< Using shader */
        Shader* shader_;

        // Transform
        Transform transform_;
};

#endif // MESHRENDERER_H
