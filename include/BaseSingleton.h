#ifndef BASESINGLETON_H
#define BASESINGLETON_H

template <class T>
class BaseSingleton
{
    protected:
    static T* self_;
public:
    static T* get()
    {
        if(self_ == nullptr) self_ = new T();
        return self_;
    }


    static void release()
    {
        if(self_ != nullptr)
            delete self_;
        self_ = nullptr;
    }



private:
/*
    BaseSingleton(const BaseSingleton& rhs) = delete;
    BaseSingleton(const BaseSingleton&& rhs) = delete;
    BaseSingleton& operator=(const BaseSingleton& rhs) = delete;
    BaseSingleton& operator=(const BaseSingleton&& rhs) = delete;
    */
};

template <class T>
T* BaseSingleton<T>::self_ = nullptr;

#endif // BASESINGLETON_H
