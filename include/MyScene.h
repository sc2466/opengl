#ifndef MYSCENE_H
#define MYSCENE_H

#include "Scene.h"

class MyScene : public Scene
{
    public:
        MyScene();
        ~MyScene();

        override void Init();

        override void Tick();

    protected:
        FtFont ft;

        Shader textSh;

        FPSCamera* fps_camera;
    private:
};

#endif // MYSCENE_H
