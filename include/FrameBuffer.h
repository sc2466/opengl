#ifndef FRAMEBUFFER_H
#define FRAMEBUFFER_H

#include "Shader.h"
#include "Camera.h"
#include "QuadMesh.h"

class GameWindow;

class FrameBuffer
{
    public:
        FrameBuffer(uint width, uint height);
        virtual ~FrameBuffer();

        /**< get window's height width and Gen framebuffer */
        void Create();

        /**< Delete framebuffer */
        void Delete();

        /**<  set frame buffer size */
        inline void setSize(uint width, uint height)
        {width_ = width; height_ = height;}

        /**< Bind this FBO */
        inline void BindFBO()
        {glBindFramebuffer(GL_FRAMEBUFFER, fbo_);}

        /**< Unbind this FBO */
        inline void UnBindFBO()
        {glBindFramebuffer(GL_FRAMEBUFFER, 0);}

        uint getFbo() const {return fbo_;}
        operator uint() const {return fbo_;}

    protected:
        /**< Create Textures */
        void CreateTextures(uint tex_count);


        /**< For debug */
        inline bool CheckFramebufferStatus(){
            #ifdef _DEBUG
            GLenum fboStatus = glCheckFramebufferStatus(GL_FRAMEBUFFER);
            if (fboStatus != GL_FRAMEBUFFER_COMPLETE){
                switch(fboStatus){
                case GL_FRAMEBUFFER_UNDEFINED:
                    ErrorPrint("윈도우가 없다.");
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
                    ErrorPrint("각 어태치먼트의 상태를 확인한다.");
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
                    ErrorPrint("적어도 하나의 버퍼를 FBO에 어태치시킨다..");
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
                    ErrorPrint("glDrawBuffers로 활성화된 모든 어태치먼트가 FBO에 존재하는지 확인한다.");
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
                    ErrorPrint("glReadBuffer로 지정한 버퍼가 FBO에 존재하는지 확인한다.");
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
                    ErrorPrint("각 어태치먼트의 샘플 개수가 동일한지 확인한다.");
                    break;
                case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
                    ErrorPrint("각 어태치먼트에 대한 레이어 수가 동일한지 확인한다.");
                    break;
                case GL_FRAMEBUFFER_UNSUPPORTED:
                    ErrorPrint("어태치된 버퍼에 대한 포맷을 확인한다.");
                    break;
                }
                std::cout << "Framebuffer not complete!" << std::endl;
                return false;
            }
            #endif // _DEBUG
            return true;
        }


        uint width_ = 0;
        uint height_ = 0;
        uint fbo_ = 0;
        uint* texArray_ = nullptr;
        uint texCount_ = 0;

    private:
};

#endif // FRAMEBUFFER_H
