#ifndef FPSCAMERA_H
#define FPSCAMERA_H

#include "Transform.h"
#include "Camera.h"

class GameWindow;

class FPSCamera
{
    public:
        FPSCamera(Camera* camera);

        void update();

    protected:
        Transform trans_;
        Camera* camera_;

        float mouseSpeed = .1f;
        float moveSpeed = 2.f;
};

#endif // FPSCAMERA_H
