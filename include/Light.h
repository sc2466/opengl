#ifndef LIGHT_H
#define LIGHT_H

#include "myHeaders.h"
#include "Shader.h"

class Light
{
public:
    enum class LightType;

    // Add to light list
    // So Management light in LightManager class
    Light(const LightType type);

    // Add to light list
    // So Management light in LightManager class
    Light(const LightType type, const Vec4& pos, const Vec3& color = Vec3(1));

    // Do nothing yet
    virtual ~Light();


    void setPosition(const Vec3& pos);
    void setColor(const Vec3& color);

    const Vec3& getPosition() const;
    const Vec3& getColor() const;
    const LightType getType() const;

protected:

    LightType type_;

    Vec3 position_;
    Vec3 color_;

    // 이번 프레임이 끝나고 다음 프레임이 시작할 때
    // 변경사항이 있다면 id 초기화
    // 'light_id_' use for light manager Vector array
    uint light_id_;

    Shader* shadowmap_shader = nullptr;

public:
    enum class LightType
    {
        none = 0,
        direction = 1,
        point = 2,
        spot = 3
    };
};



#endif // LIGHT_H
