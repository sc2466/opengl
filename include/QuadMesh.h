#ifndef QUADMESH_H
#define QUADMESH_H

#include "Mesh.h"


class QuadMesh : public Mesh
{
    public:
        QuadMesh();
        virtual ~QuadMesh();

        static QuadMesh* get(){
            if(quadMesh_ == nullptr) quadMesh_ = new QuadMesh;
            return quadMesh_;
        }

        void Render();

    protected:


        static uint vao_;
        static uint vbo_;

        static QuadMesh* quadMesh_;
};

#endif // QUADMESH_H
