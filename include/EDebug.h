#ifndef EDEBUG_H
#define EDEBUG_H

#include "myHeaders.h"

#define DebugPrint(x) (std::cout << __FILE__ << ":" << __LINE__ << ": "<< x << std::endl)
#define LogPrint(x) (std::cout << __FILE__ << ":" << __LINE__ << ": "<< x << std::endl)
#define ErrorPrint(x) (std::cout << __FILE__ << ":" << __LINE__ << ": "<< x << std::endl)

class EDebug
{
public:
/*
    inline static void Print(const String& value)
    {
        std::cout << __FILE__ << ":" << __LINE__ << ": "<< value.c_str() << std::endl;
    }
    inline static void Print(const int value)
    {
        Print(intToString(value));
    }
    inline static void Print(const float value)
    {
        Print(floatToString(value));
    }
*/
    static void PrintOpenglState();

protected:
};

#endif // EDEBUG_H
