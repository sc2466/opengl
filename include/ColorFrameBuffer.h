#ifndef COLORFRAMEBUFFER_H
#define COLORFRAMEBUFFER_H

#include "FrameBuffer.h"

class ColorFrameBuffer : public FrameBuffer
{
    public:
        ColorFrameBuffer(uint width, uint height);
        virtual ~ColorFrameBuffer();

        void Create();

        void Render() const;
};

#endif // COLORFRAMEBUFFER_H
