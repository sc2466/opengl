#ifndef COMPONENT_H
#define COMPONENT_H


// Component interface
class Component
{
    public:
        Component();
        virtual ~Component();

        virtual void init(){}

        virtual void tick(){}
};

#endif // COMPONENT_H
