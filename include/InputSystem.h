#ifndef INPUTSYSTEM_H
#define INPUTSYSTEM_H

#include "BaseSingleton.h"
#include "Types.h"

#define KEYNUM 348

class GameWindow;
class GLFWwindow;

class InputSystem : public BaseSingleton<InputSystem>
{
    friend GameWindow;
public:

    void processInput(GameWindow* game_window);

    static void keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods);
    static void mouseCallback(GLFWwindow* window, double xpos, double ypos);
    static void mouseButtonCallback(GLFWwindow* _window, int button, int action, int mods);

    inline const Vec2 getMouseOffset() const {return Vec2(mouse_x_offset_, mouse_y_offset_);}
    inline const float getMouseXOffset() const {return mouse_x_offset_;}
    inline const float getMouseYOffset() const {return mouse_y_offset_;}

    inline const Vec2 getMousePos() const {return Vec2(mouse_last_x_, mouse_last_y_);}
    inline const float getMouseX() const {return mouse_last_x_;}
    inline const float getMouseY() const {return mouse_last_y_;}

    inline const uchar getKey(uint i) const {return keyButtons[i];}

protected:
    static bool first_mouse_call_;
    static bool mouseButtons[8];
    static float mouse_x_offset_, mouse_y_offset_;
    static float mouse_last_x_, mouse_last_y_;
    static uchar keyButtons[KEYNUM];

    unsigned short kbButtonQueue_[32];
};

#endif // INPUTSYSTEM_H
