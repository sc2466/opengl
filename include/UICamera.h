#ifndef UICAMERA_H
#define UICAMERA_H

#include "myHeaders.h"
#include "GameWindow.h"

class UICamera
{
    public:
        UICamera(const GameWindow* window);

        Mat4& getMat4();
    protected:
        Mat4 orthProj_;

        const uint &width_, &height_;
};

#endif // UICAMERA_H
