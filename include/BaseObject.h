#ifndef BASEOBJECT_H
#define BASEOBJECT_H

#include "myHeaders.h"

class BaseObject
{
    public:
        BaseObject();
        virtual ~BaseObject();


        uint getID(){return id_;}
        void setID(uint id){id_=id;}
    protected:
        uint id_;
    private:
};

#endif // BASEOBJECT_H
