#ifndef LIGHTMANAGER_H
#define LIGHTMANAGER_H

#include "BaseSingleton.h"
#include "Manager.h"
#include "PointLight.h"

class LightManager : public Manager<Light>, BaseSingleton<LightManager>
{
    public:
        LightManager();
        virtual ~LightManager();

        // Add light
        void AddLight(LightType type);

        // Remove light
        // TODO (180416) : Use Quque
        void RemoveLight(uint light_id, LightType type);

        // set light parameter to shader
        void ApplyLightPropertyToShader(const Shader* shader);

        // Remove All
        void RemoveAll();

        // Render Shadow Maps
        void RenderShadowMaps();

    protected:

        void SortLightsList();

        // Removed lights count in this tick or frame
        // For What?
        // 이번 프레임이 끝나고 다음 프레임이 시작할 때
        // 변경사항이 있다면 id 초기화
        // 'light_id_' use for light manager Vector array
        uint removed_light_count_in_this_tick;


        // Lights Vectors
        // Direction light Vector
        Vector<DirectionalLight> direc_light_vec_;

        // Point light Vector
        Vector<PointLight> point_light_vec_;

        // Spot light Vector
        //Vector<SpotLight*> light_vec_;
    private:
};

#endif // LIGHTMANAGER_H
