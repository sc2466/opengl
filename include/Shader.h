#ifndef SHADER_H
#define SHADER_H

#include "myHeaders.h"

#include "Texture.h"

class Shader;

class BaseShaderProperty
{
public:
    BaseShaderProperty(const uint shader_id, const uint hash_t)
        : shader_id_(shader_id), hash_(hash_t) {}

    virtual ~BaseShaderProperty();

    const uint getHash() const
    {
        return hash_;
    }
    const uint getID() const
    {
        return id_;
    }
    const String& getName() const
    {
        return name_;
    }

    // virtual bind for manager loop
    virtual void bind() const = 0;

protected:

    // Use for find fast
    uint hash_;

    // Shader program ID
    const uint shader_id_;

    // Shader uniform location id
    uint id_;

    // Shader uniform name
    String name_;

    // Is changed fixed value
    bool is_changed_;

    friend Shader;
};

class Shader
{
public:
    Shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath = 0);

    Shader(const Shader& rhs);

    ~Shader();

    void createShader(const char* vertexPath, const char* fragmentPath, const char* geometryPath);

    void use() const;
    void useProgram() const
    {
        glUseProgram(id_);
    }

    void disable() const
    {
        glUseProgram(0);
    }

    // set property value
    template <class T>
    void setPropertyFixedValue(const String& name, const T* value);
    //template <class T>
    //void setPropertyFixedValue(const uint t_hash, const T* value);

    // set property value
    template <class T>
    void setPropertyReferenceValue(const String& name, const T* value);
    //template <class T>
    //void setPropertyReferenceValue(const uint t_hash, const T* value);

    // Find property and return that
    // TODO (180416) : Need more speed
    BaseShaderProperty* FindProperty(const String& name) const;

    /* OLD WAY */
    void setBool(const String &name, bool value) const;
    void setInt(const String &name, int value) const;
    void setFloat(const String &name, float value) const;
    void setVec2(const String &name, const glm::vec2 &value) const;
    void setVec2(const String &name, float x, float y) const;
    void setVec3(const String &name, const Vec3 &value) const;
    void setVec3(const String &name, float x, float y, float z) const;
    void setVec4(const String &name, const glm::vec4 &value) const;
    void setVec4(const String &name, float x, float y, float z, float w);
    void setMat2(const String &name, const glm::mat2 &mat) const;
    void setMat3(const String &name, const glm::mat3 &mat) const;
    void setMat4(const String &name, const glm::mat4 &mat) const;

    inline GLuint getUniformID(const String& name) const
    {return glGetUniformLocation(id_, name.c_str());}


    // Add texture to shader
    void addTexture(Texture* texture, const String& name = "tex");

    // Delete all textures
    void deleteAllTexture();

    static const String& getArrayChar(const String& name, const unsigned i, const String& propertyName);

    operator uint() const
    {
        return id_;
    }
    uint get() const
    {
        return id_;
    }


protected:
    // utility function for checking shader compilation/linking errors.
    // ------------------------------------------------------------------------
    GLint checkCompileErrors(GLuint shader, String type, String name);


    // bind all Texture
    inline void bindTextures() const
    {
        uint tex_vec_size = tex_vec_.size();
        for(uint i = 0; i < tex_vec_size; ++i)
        {
            glActiveTexture(GL_TEXTURE0+i); // activate the tex unit first before binding tex
            glBindTexture(GL_TEXTURE_2D, *tex_vec_[i]);
        }
    }

    //Program ID
    uint id_;

    bool is_compiled_ = false;

    Vector<BaseShaderProperty> property_vec_;

    Vector<Texture*> tex_vec_;
    Vector<Texture*>::iterator tex_vec_iter_;

public:
    // inline functions
    inline void setBool(const uint uniform_id, bool value) const
    {glUniform1i(uniform_id, (int)value);}

    inline void setInt(const uint uniform_id, int value) const
    {glUniform1i(uniform_id, value);}

    inline void setFloat(const uint uniform_id, float value) const
    {glUniform1f(uniform_id, value);}

    inline void setVec2(const uint uniform_id, const glm::vec2 &value) const{
        glUniform2fv(uniform_id, 1, &value[0]);
    }
    inline void setVec2(const uint uniform_id, float x, float y) const    {
        glUniform2f(uniform_id, x, y);
    }
    inline void setVec3(const uint uniform_id, const Vec3 &value) const    {
        glUniform3fv(uniform_id, 1, &value[0]);
    }
    inline void setVec3(const uint uniform_id, float x, float y, float z) const    {
        glUniform3f(uniform_id, x, y, z);
    }
    inline void setVec4(const uint uniform_id, const glm::vec4 &value) const    {
        glUniform4fv(uniform_id, 1, &value[0]);
    }
    inline void setVec4(const uint uniform_id, float x, float y, float z, float w)    {
        glUniform4f(uniform_id, x, y, z, w);
    }
    inline void setMat2(const uint uniform_id, const glm::mat2 &mat) const  {
        glUniformMatrix2fv(uniform_id, 1, GL_FALSE, &mat[0][0]);
    }
    inline void setMat3(const uint uniform_id, const glm::mat3 &mat) const    {
        glUniformMatrix3fv(uniform_id, 1, GL_FALSE, &mat[0][0]);
    }
    inline void setMat4(const uint uniform_id, const glm::mat4 &mat) const    {
        glUniformMatrix4fv(uniform_id, 1, GL_FALSE, &mat[0][0]);
    }
};
#endif
