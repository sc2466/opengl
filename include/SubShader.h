#ifndef SUBSHADER_H
#define SUBSHADER_H

//TODO Delete this class!!
#include "Shader.h"

class SubShader : public Shader
{
    public:
        static Shader* getSpriteShader()
        {
            if(sprite_shader_ != nullptr) return sprite_shader_;
            sprite_shader_ = new Shader("shader/SimpleShaderVS.txt", "shader/SimpleShaderFS.txt");
        }

        static Shader* getDeferredGeoShader()
        {
            if(deferred_geo_shader_ != nullptr) return deferred_geo_shader_;
            deferred_geo_shader_ = new Shader("shader/deferredGeo_vs.txt","shader/deferredGeo_fs.txt");
        }

        static Shader* getDeferredShader()
        {
            if(deferred_shader_ != nullptr) return deferred_shader_;
            deferred_shader_ = new Shader("shader/DeferredShader_vs.txt","shader/DeferredShader_fs.txt");
        }

        static Shader* getShadowMapShader()
        {
            if(shadow_shader_ != nullptr) return shadow_shader_;
            shadow_shader_ = new Shader("shader/DepthMapShader_vs.txt", "shader/DepthMapShader_fs.txt");
        }

        static Shader* getTextShader()
        {
            if(textShader_ != nullptr) return textShader_;
            textShader_ = new Shader("shader/text_vs.txt", "shader/text_fs.txt");
        }



    protected:
        SubShader() = delete;
        ~SubShader() = delete;

        static Shader* shadow_shader_;
        static Shader* deferred_geo_shader_;
        static Shader* deferred_shader_;
        static Shader* sprite_shader_;
        static Shader* textShader_;
};

#endif // SUBSHADER_H
