#ifndef TRANSFORM_H
#define TRANSFORM_H

#include "Types.h"
#include "EDebug.h"

class Transform
{
    public:
        Transform(const Vec3& pos = Vec3(0), const Vec3& rot = Vec3(0), const Vec3& scale = Vec3(1));

        virtual void log()
        {
            LogPrint(position_.x << ","<< position_.y << ","<< position_.z);
        }

        inline const Vec3& getPos() const {return position_;}
        inline const Vec3& getRot() const {return rotation_;}
        inline const Vec3& getScale() const {return scale_;}

        const Mat4& get();

        inline operator Mat4()
        {
            return get();
        }

    protected:
        Mat4 mat4_;

        Vec3 position_;
        Vec3 rotation_;
        Vec3 scale_;
    private:
};

#endif // TRANSFORM_H
