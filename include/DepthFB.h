#ifndef DEPTHFB_H
#define DEPTHFB_H

#include "FrameBuffer.h"
#include "DepthTexture.h"

class DepthFB : public FrameBuffer
{
    public:
        DepthFB(uint width, uint height);
        virtual ~DepthFB();

        void Create();

        void Render() const;

        const uint getDepthTex() const
        {
            return *depth_tex_;
        }

    private:
        DepthTexture* depth_tex_ = nullptr;
};

#endif // DEPTHFB_H
