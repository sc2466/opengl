#ifndef FONT_H
#define FONT_H

#include "myHeaders.h"

class Font
{
public:

    void init(){
        initShader(); initVertex();
    }
    void useShader();

protected:

    // Configure shader
    void initShader();
    // Configure VAO/VBO for texture quads
    void initVertex();

    uint vao_ = 0, vbo_ = 0;
};

#endif // FONT_H
