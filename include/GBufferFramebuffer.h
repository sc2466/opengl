#ifndef GBUFFERFRAMEBUFFER_H
#define GBUFFERFRAMEBUFFER_H

#include "FrameBuffer.h"

class GBufferFramebuffer : public FrameBuffer
{
    public:
        GBufferFramebuffer(uint width, uint height);
        virtual ~GBufferFramebuffer();

        /**< get window's height width and Gen framebuffer */
        void Create();

        /**< Init shader uniforms */
        void InitShaderValue(Shader* shader);

        /**< Render to Quad mesh */
        void Render() const;

        uint getDepthTex();

    protected:

    private:
};

#endif // GBUFFERFRAMEBUFFER_H
