#ifndef SPHEREMESH_H
#define SPHEREMESH_H

#include "Mesh.h"

class SphereMesh : public Mesh
{
    public:
        virtual ~SphereMesh();

        void Render();

        static void Create();

        static SphereMesh* get();

    protected:
        static SphereMesh* self_;
        static uint sphereVAO_;
        static uint sphereVBO_;
        static uint indexCount_;

    private:
        SphereMesh();
};

#endif // SPHEREMESH_H
