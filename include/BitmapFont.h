#ifndef BITMAPFONT_H
#define BITMAPFONT_H

#define ASCIICOUNT 256
#define UNICODECOUNT 65536

#include "Font.h"
#include "Texture.h"

class BitmapFont : public Font
{
public:
    BitmapFont(const String& path);
    virtual ~BitmapFont();

    bool parseFont(const String& path, Charset& CharsetDesc);

    void render(const String& text, const Vec2& pos = Vec2(0,0),
                 GLfloat scale = 1, const Vec3& color = Vec3(0.0, 0.0f, 0.0f));

protected:

    void init();

    Texture fontTex_;
    Charset charSet_;


    struct Vertex{
        float x, y;
        float tx, ty;
    };

    struct CharDescriptor
    {
        //clean 16 bytes
        ushort x, y;
        ushort Width, Height;
        ushort XOffset, YOffset;
        ushort XAdvance;
        ushort Page;

        CharDescriptor() : x( 0 ), y( 0 ), Width( 0 ), Height( 0 ), XOffset( 0 ), YOffset( 0 ),
            XAdvance( 0 ), Page( 0 )
        { }
    };

    struct Charset
    {
        ushort LineHeight;
        ushort Base;
        ushort Width, Height;
        ushort Pages;
        CharDescriptor Chars[UNICODECOUNT];
    };
};

#endif // BITMAPFONT_H
