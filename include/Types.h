#ifndef TYPES_H
#define TYPES_H

#include "memory/Types.h"

// define types
#include <Vector>
#define Vector std::vector

#include <String>
#define String std::string

#include <list>
#define List std::list

#include <map>
#define Map std::map


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
typedef glm::vec2 Vec2;
typedef glm::vec3 Vec3;
typedef glm::vec4 Vec4;
typedef glm::mat4 Mat4;
typedef glm::mat3 Mat3;
typedef glm::mat2 Mat2;


typedef unsigned uint;
typedef unsigned char uchar;
typedef unsigned short ushort;

#endif // TYPES_H
