#ifndef TIMECHECKER_H
#define TIMECHECKER_H

#include <iostream>
#include <cmath>
#include <chrono>


class TimeChecker
{
    public:
        TimeChecker();
        virtual ~TimeChecker();

        void setStartTime();
        void setEndTime();

        void PrintDoubleNanoSec();
        void PrintNanoSec();

        std::chrono::nanoseconds getNanoSec();
        std::chrono::milliseconds getMilliSec();
        double getDoubleNanoSec();

    protected:
        std::chrono::system_clock::time_point startPoint, endPoint;
    private:
};

#endif // TIMECHECKER_H
