#ifndef DIRECTIONALLIGHT_H
#define DIRECTIONALLIGHT_H

#include "Light.h"

#include "DepthTexture.h"

class Shader;
class Mesh;

class DirectionalLight : public Light
{
public:
    DirectionalLight(const Vec3& direction = Vec3(1));
    virtual ~DirectionalLight();

    // set uniform Light matrix to shader
    void setLightMatToShader(Shader* shader);

    // Render every meshs and draw meshs shadow depth to texture toward FBO
    // 프레임 버퍼를 통해 모든 매쉬의 그림자 깊이 맵을 그린다.
    void RenderShadowMap();


    const Vec3& getDirection() const;
    void setDirection(const Vec3& direction);

    const Mat4& getMat4() const;


protected:

    // Create shadow map And bind to FBO
    void CreateShadowMap();

    // light projection view matrix
    Mat4 light_vp_mat_;

    // direction for direction light
    Vec3 direction_;

    // use for make shadow
    uint shadow_width_ = 1024;
    uint shadow_height_ = 1024;
    uint depth_fbo_ = 0;
    DepthTexture* shadow_tex_ = nullptr;

private:
    Mat4 lightProjection;
    Mat4 lightView;
    Mat4 lightSpaceMatrix;
    const float near_plane = 1.0f, far_plane = 7.5f;
};

#endif // DIRECTIONALLIGHT_H
