#ifndef DEPTHTEXTURE_H
#define DEPTHTEXTURE_H

#include "Texture.h"

class DepthTexture : public Texture
{
    public:
        DepthTexture(uint width, uint height);
        ~DepthTexture();

        void Create();
};

#endif // DEPTHTEXTURE_H
