#ifndef SCENE_H
#define SCENE_H

#include "BaseSingleton.h"
#include "FPSCamera.h"

#include "MeshRendererManager.h"
#include "InputSystem.h"

class GameWindow;
class FtFont;

class Scene
{
public:
    // get width height
    Scene(GameWindow* window);

    void init();

    void loop();

    // ------------------------------------------------
    // get set

    inline GameWindow* getGameWindow() {return window_;}
    inline Camera& getMainCamera() {return camera_;}

    inline float getDeltaTime() const {return deltaTime_;}

    // Frame per sec
    inline float getFPS() const {return fps_;}

    // Frame per sec Update per 0.1sec
    // Can Use for debug value
    inline float getDelayFPS() const {return delay_fps_;}

    inline static Scene* get(){return self_;}

protected:

    Camera camera_;
    GameWindow* window_;

    const uint &width_, &height_;

    float deltaTime_ = 0.0f;
    float fps_ = 0;
    float delay_fps_ = 0;

    uint is_active = 1;

    MeshRendererManager meshRendererManager;

    FtFont* ft;
    FPSCamera* fCamera;

private:
    static Scene* self_;
};

#endif // RENDERENGINE_H
