#ifndef MESH_H
#define MESH_H

#include "myHeaders.h"

class CubeMesh;
class QuadMesh;
class SphereMesh;

class Mesh
{
    public:
        Mesh();
        virtual ~Mesh();

        virtual void Render();

        void setVertex(float* vertiecs, uint m_size);
        void setVertexToAttrib(unsigned location, unsigned count, unsigned data_block_size, unsigned start_offset, unsigned short dataType = GL_FLOAT);

    protected:
        uint vao_ = 0, vbo_ = 0;
        uint vertexCount_ = 0;

        friend CubeMesh;
        friend QuadMesh;
        friend SphereMesh;
};


#endif // MESH_H
