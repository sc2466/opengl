#ifndef MYHEADERS_H_INCLUDED
#define MYHEADERS_H_INCLUDED

#include <iostream>

#include "Types.h"

#include <glad/glad.h>
#include <glfw3.h>


const String intToString(int n);
const String floatToString(float n);
const String getArrayChar(const String& name, const unsigned i, const String& propertyName);

#endif // MYHEADERS_H_INCLUDED
