#ifndef FTFONT_H
#define FTFONT_H

#include <ft2build.h>
#include FT_FREETYPE_H

#include "Font.h"


class FtFont : public Font
{
    protected:
    struct Character;

    public:
        FtFont(const String& font_path = "fonts/NanumSquareRoundR.ttf");
        virtual ~FtFont();

        void renderText(const String& text, const Vec2& pos = Vec2(0,0),
                         float scale = 1, const Vec3& color = Vec3(0.0, 0.0f, 0.0f));

    protected:

        void init(const String& font_path);
        Map<GLchar, Character> Characters;

    protected:
    struct Character {
        GLuint TextureID;   // ID handle of the glyph texture
        glm::ivec2 Size;    // Size of glyph
        glm::ivec2 Bearing;  // Offset from baseline to left/top of glyph
        GLuint Advance;    // Horizontal offset to advance to next glyph
    };
};

#endif // FTFONT_H
