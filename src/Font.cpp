#include "Font.h"
#include "myHeaders.h"
#include "SubShader.h"
#include "GameWindow.h"

void Font::initShader()
{
    static uint a;
    if(a==1) return;
    Shader* textSh = SubShader::getTextShader();
    textSh->useProgram();
    const float w = (float)GameWindow::get()->getWidth();
    const float h = (float)GameWindow::get()->getHeight();
    Mat4 orthProj = glm::ortho(0.f, w, 0.f, h);
    textSh->setInt("text", 0);
    textSh->setMat4("proj", orthProj);

    a = 1;
}
void Font::initVertex()
{
    glGenVertexArrays(1, &vao_);
    glGenBuffers(1, &vbo_);
    glBindVertexArray(vao_);
    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * 6 * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}
void Font::useShader(){
    SubShader::getTextShader()->useProgram();
}
