#include "Shader.h"
#include <fstream>
#include <sstream>

Shader::Shader(const char* vertexPath, const char* fragmentPath, const char* geometryPath)
{
    createShader(vertexPath, fragmentPath, geometryPath);
}

Shader::Shader(const Shader& rhs)
    : id_(rhs.id_)
{
}

Shader::~Shader()
{
    glDeleteProgram(id_);
    deleteAllTexture();
}

void Shader::createShader(const char* vertexPath, const char* fragmentPath, const char* geometryPath)
{
    if(is_compiled_) return;

    String vertexCode;
    String fragmentCode;
    String geometryCode;
    std::ifstream vShaderFile;
    std::ifstream fShaderFile;
    std::ifstream gShaderFile;
    // ensure ifstream objects can throw exceptions:
    vShaderFile.exceptions (std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
    fShaderFile.exceptions (std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
    gShaderFile.exceptions (std::ifstream::eofbit | std::ifstream::failbit | std::ifstream::badbit);
    try
    {
        // open files
        vShaderFile.open(vertexPath);
        fShaderFile.open(fragmentPath);
        std::stringstream vShaderStream, fShaderStream;
        // read file's buffer contents into streams
        vShaderStream << vShaderFile.rdbuf();
        fShaderStream << fShaderFile.rdbuf();
        // close file handlers
        vShaderFile.close();
        fShaderFile.close();
        // convert stream into String
        vertexCode   = vShaderStream.str();
        fragmentCode = fShaderStream.str();
        if(geometryPath != 0)
        {
            gShaderFile.open(geometryPath);
            std::stringstream gShaderStream;
            gShaderStream << gShaderFile.rdbuf();
            gShaderFile.close();
            geometryCode = gShaderStream.str();
        }
    }
    catch (std::ifstream::failure e)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
        id_ = 0;
        return;
    }
    catch (...)
    {
        std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ : " << vertexPath << std::endl;
        id_ = 0;
        return;
    }
    const char* vShaderCode = vertexCode.c_str();
    const char * fShaderCode = fragmentCode.c_str();
    // 2. compile shaders
    uint vertex, fragment;
    char infoLog[512];
    // vertex shader
    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vShaderCode, NULL);
    glCompileShader(vertex);
    checkCompileErrors(vertex, "VERTEX", vertexPath);
    // fragment Shader
    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fShaderCode, NULL);
    glCompileShader(fragment);
    checkCompileErrors(fragment, "FRAGMENT", vertexPath);
    // if geometry shader is given, compile geometry shader
    uint geometry;
    if(geometryPath != 0)
    {
        const char * gShaderCode = geometryCode.c_str();
        geometry = glCreateShader(GL_GEOMETRY_SHADER);
        glShaderSource(geometry, 1, &gShaderCode, NULL);
        glCompileShader(geometry);
        checkCompileErrors(geometry, "GEOMETRY", vertexPath);
    }
    // shader Program
    id_ = glCreateProgram();
    glAttachShader(id_, vertex);
    glAttachShader(id_, fragment);
    if(geometryPath != 0)
        glAttachShader(id_, geometry);
    glLinkProgram(id_);
    GLint t_success = checkCompileErrors(id_, "PROGRAM", vertexPath);
    // delete the shaders as they're linked into our program now and no longer necessery
    glDeleteShader(vertex);
    glDeleteShader(fragment);
    if(geometryPath != 0)
        glDeleteShader(geometry);
    if(t_success)
        std::cout << "Success Build Shader : " << vertexPath << std::endl;
    else
        std::cout << "Fail Build Shader : " << vertexPath << std::endl;
    std::cout << "----------------------------------------------------- --\n\n";
    is_compiled_ = true;
}

void Shader::use() const
{
#ifdef _DEBUG
    if(!id_)
        std::cout << "Shader id is 0 " << id_ << std::endl;
#endif // _DEBUG
    // Remove (180504)
    // bind uniform property
    //bindPropertys();

    // bind texture
    bindTextures();

    glUseProgram(id_);
}


GLint Shader::checkCompileErrors(GLuint shader, String type, String name)
{
    GLint success;
    GLchar infoLog[1024];
    if(type != "PROGRAM")
    {
        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
        if(!success)
        {
            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::SHADER_COMPILATION_ERROR "<<name<<" of type: " << type << "\n" << infoLog << "-- --------------------------------------------------- -- " << std::endl;
        }
    }
    else
    {
        glGetProgramiv(shader, GL_LINK_STATUS, &success);
        if(!success)
        {
            glGetProgramInfoLog(shader, 1024, NULL, infoLog);
            std::cout << "ERROR::PROGRAM_LINKING_ERROR "<<name<<" of type: " << type << "\n" << infoLog << "-- --------------------------------------------------- -- " << std::endl;
        }
    }
    return success;
}

void Shader::setBool(const String &name, bool value) const
{
    glUniform1i(getUniformID(name), (int)value);
}
void Shader::setInt(const String &name, int value) const
{
    glUniform1i(getUniformID(name), value);
}
void Shader::setFloat(const String &name, float value) const
{
    glUniform1f(getUniformID(name), value);
}
void Shader::setVec2(const String &name, const Vec2 &value) const
{
    glUniform2fv(getUniformID(name), 1, &value[0]);
}
void Shader::setVec2(const String &name, float x, float y) const
{
    glUniform2f(getUniformID(name), x, y);
}
void Shader::setVec3(const String &name, const Vec3 &value) const
{
    glUniform3fv(getUniformID(name), 1, &value[0]);
}
void Shader::setVec3(const String &name, float x, float y, float z) const
{
    glUniform3f(getUniformID(name), x, y, z);
}
void Shader::setVec4(const String &name, const Vec4 &value) const
{
    glUniform4fv(getUniformID(name), 1, &value[0]);
}
void Shader::setVec4(const String &name, float x, float y, float z, float w)
{
    glUniform4f(getUniformID(name), x, y, z, w);
}
void Shader::setMat2(const String &name, const Mat2 &mat) const
{
    glUniformMatrix2fv(getUniformID(name), 1, GL_FALSE, &mat[0][0]);
}
void Shader::setMat3(const String &name, const Mat3 &mat) const
{
    glUniformMatrix3fv(getUniformID(name), 1, GL_FALSE, &mat[0][0]);
}
void Shader::setMat4(const String &name, const Mat4 &mat) const
{
    glUniformMatrix4fv(getUniformID(name), 1, GL_FALSE, &mat[0][0]);
}


void Shader::addTexture(Texture* tex, const String& name)
{
    tex_vec_.push_back(tex);
    uint&& tex_size = tex_vec_.size()-1;
    std::stringstream ss;
    ss << name << tex_size;
    setInt(ss.str(), tex_size);
}


void Shader::deleteAllTexture()
{
    Vector<Texture*>().swap(tex_vec_);
}
