#include "Mesh.h"

Mesh::Mesh()
{
    //ctor
}

Mesh::~Mesh()
{
    //dtor
    glDeleteVertexArrays(1, &vao_);
    glDeleteBuffers(1, &vbo_);
}

void Mesh::Render()
{
    if(vao_ == 0) return;
    glBindVertexArray(vao_);
    glDrawArrays(GL_TRIANGLES, 0, vertexCount_);
}

void Mesh::setVertex(float* vertiecs, uint m_size)
{
    vertexCount_ = m_size;
    glGenVertexArrays(1, &vao_);
	glGenBuffers(1, &vbo_);

	glBindVertexArray(vao_);
	glBindBuffer(1, vbo_);

	glBindBuffer(GL_ARRAY_BUFFER, vbo_);
	glBufferData(GL_ARRAY_BUFFER, m_size, vertiecs, GL_STATIC_DRAW);
}

void Mesh::setVertexToAttrib(unsigned location, unsigned count, unsigned data_block_size, unsigned start_offset, unsigned short dataType)
{
    glVertexAttribPointer(location, count, GL_FLOAT,GL_FALSE,data_block_size, (void*)start_offset);
	glEnableVertexAttribArray(location);
}
