#include "EDebug.h"

void EDebug::PrintOpenglState()
{
    GLint major, minor;
    glGetIntegerv(GL_MAJOR_VERSION, &major);
    glGetIntegerv(GL_MINOR_VERSION, &minor);
    std::cout << "GL_VERSION: " << glGetString(GL_VERSION) <<std::endl;
    std::cout << "GL_SHADING_LANGUAGE_VERSION: " << glGetString(GL_SHADING_LANGUAGE_VERSION) << std::endl;
    std::cout << "GL_RENDERER: " << glGetString(GL_RENDERER) << std::endl;
    //std::cout << "GL_EXTENSIONS: " << glGetString(GL_EXTENSIONS) << std::endl;
    std::cout << "GL_VENDOR: " << glGetString(GL_VENDOR) << std::endl;
    printf("GL Version(Interger) : %d.%d\n", major,minor);
}
