#include "InputSystem.h"
#include "GameWindow.h"
#include "EDebug.h"
#include <glfw3.h>

void InputSystem::processInput(GameWindow* game_window)
{
    GLFWwindow* window = game_window->getGLFWWindow();
}

void InputSystem::keyCallback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
    if(key < 0) return;

    keyButtons[key] = action;

    LogPrint(key);

    if(key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
}

void InputSystem::mouseCallback(GLFWwindow* window, double xpos, double ypos)
{
    if (first_mouse_call_)
    {
        mouse_last_x_ = xpos;
        mouse_last_y_ = ypos;
        first_mouse_call_ = false;
    }

    mouse_x_offset_ = xpos - mouse_last_x_;
    mouse_y_offset_ = mouse_last_y_ - ypos; // reversed since y-coordinates go from bottom to top

    mouse_last_x_ = xpos;
    mouse_last_y_ = ypos;
}

void InputSystem::mouseButtonCallback(GLFWwindow* _window, int button, int action, int mods)
{
    if (action == GLFW_PRESS)
        mouseButtons[button] = true;
    else if (action == GLFW_RELEASE)
        mouseButtons[button] = false;
}

bool InputSystem::first_mouse_call_ = true;
bool InputSystem::mouseButtons[8];
float InputSystem::mouse_x_offset_, InputSystem::mouse_y_offset_;
float InputSystem::mouse_last_x_, InputSystem::mouse_last_y_;
uchar InputSystem::keyButtons[GLFW_KEY_LAST];
