#include "MeshRendererManager.h"

void MeshRendererManager::create(Mesh* mesh, Shader* shader){
    add(new MeshRenderer(mesh,shader));
}

void MeshRendererManager::render(Camera* camera){
    uint size = list_.size();
    for(uint i = 0; i < size; ++i)
    {
        list_[i].render(camera);
    }
}
