#include "DirectionalLight.h"
#include "SubShader.h"
#include "Mesh.h"
#include "GameWindow.h"

DirectionalLight::DirectionalLight(const Vec3& _direction)
: Light(LightType::direction)
{
    // set light type and light direction
    setDirection(_direction);
}

DirectionalLight::~DirectionalLight()
{
}


void DirectionalLight::setLightMatToShader(Shader* shader)
{
    shader->useProgram();
    uint vp = shader->getUniformID("vp");
    uint shadow = shader->getUniformID("shadow");
    shader->setMat4(vp, light_vp_mat_);
    shader->setInt(shadow, shadow_tex_->get());
}

void DirectionalLight::RenderShadowMap()
{
    // if didn't have depth frame buffer
    // Create Shadow map or not
    if(depth_fbo_ == NULL)
        CreateShadowMap();

    uint height = GameWindow::get()->getHeight();
    uint width = GameWindow::get()->getWidth();
    /*
    const Shader* shadowmap_shader = SubShader::getShadowMapShader();
    light_depth_shader->useProgram();

    // render scene from light's point of view
    light_depth_shader->setMat4("vp", light_vp_mat_);

    // Every meshs render to shadow depth texture
    // 프레임 버퍼에 라이트 시점으로 본 뎁스맵을 그린다.

    glViewport(0, 0, shadow_width_, shadow_height_);
    glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo_);
        glClear(GL_DEPTH_BUFFER_BIT);
        // Draw every meshs
        ModelManager::get()->RenderMeshs();
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glViewport(0, 0, width, height);
    */
}


const Vec3& DirectionalLight::getDirection() const
{
    return direction_;
}

void DirectionalLight::setDirection(const Vec3& direction)
{
    direction_ = direction;
    lightProjection = glm::ortho(-10.0f, 10.0f, -10.0f, 10.0f, near_plane, far_plane);

    Mat4&& lightView = glm::lookAt(-direction, glm::vec3(0.0f), glm::vec3(0.0, 1.0, 0.0));
    light_vp_mat_ = lightProjection * lightView;
}

const Mat4& DirectionalLight::getMat4() const
{
    return light_vp_mat_;
}

void DirectionalLight::CreateShadowMap()
{
    if(depth_fbo_ == 0)
        return;

    // Create Depth Framebuffer
    glGenFramebuffers(1, &depth_fbo_);

    // Create depth texture
    shadow_tex_ = new DepthTexture(shadow_width_, shadow_height_);

    // Attach depth texture to FBO's depth buffer
    glBindFramebuffer(GL_FRAMEBUFFER, depth_fbo_);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, shadow_tex_->get(), 0);
    glDrawBuffer(GL_NONE);
    glReadBuffer(GL_NONE);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}


