#include "LightManager.h"

LightManager::LightManager()
{
    //ctor
}

LightManager::~LightManager()
{
    //dtor
}

void LightManager::AddLight(Light* light)
{
    const Light::LightType&& type = light.getType();

    if(type == Light::LightType.point)
        point_light_vec_.push_back(light);
    else if(type == Light::LightType.direction)
        direc_light_vec_.push_back(light);
}

void LightManager::RemoveLight(uint light_id, LightType type)
{
    if(type == Light::LightType.point)
        point_light_vec_.erase(point_light_vec_.begin()+light_id);
    else if(type == Light::LightType.direction)
        direc_light_vec_.erase(direc_light_vec_.begin()+light_id);
}

void LightManager::ApplyLightPropertyToShader(const Shader* shader)
{
    uint direc_count = direc_light_vec_.size();
    uint spot_count = 0;
    uint point_count = point_light_vec_.size();

    shader->useProgram();

    // TODO (180401)(Cyber#1#): If change light position or other value so no need to set other lights value? You need to Do this

    //Point light
    for (uint i = 0; i < point_count; i++)
    {
        shader->setVec3("pointLights[" + intToString(i) + "].position", point_light_vec_[i].getPosition());
        shader->setVec3("pointLights[" + intToString(i) + "].color", point_light_vec_[i].getColor());
    }
    // Direc light
    for (uint i = 0; i < direc_count; i++)
    {
        shader->setVec3("direcLights[" + intToString(i) + "].direction", direc_light_vec_[i].getDirection());
        shader->setVec3("direcLights[" + intToString(i) + "].color", direc_light_vec_[i].getColor());
        shader->setMat4("direcLights[" + intToString(i) + "].mat", direc_light_vec_[i].getMat4());
#ifdef SIBALSSAJIVANG
        shader->setMat4("direcLight" + intToString(i) + ".shadow_map", direc_light_vec_[i].getMat4());
#else
        cout << "i didn't want this in sajivang" << endl;
        shader->setMat4("direcLights[" + intToString(i) + "].shadow_map", direc_light_vec_[i].getMat4());
#endif
    }
    // Spot light
    /*
    for (uint i = 0; i < light_vec_.size(); i++)
    {
        ++spot_count;
    }
    */

    shader->setInt("pointLightCount", point_count);
    shader->setInt("direcLightCount", direc_count);
    shader->setInt("spotLightCount", spot_count);

    glUseProgram(0);
}

void LightManager::RenderShadowMaps()
{
    const uint direc_count = direc_light_vec_.size();
    const uint spot_count = 0;
    const uint point_count = point_light_vec_.size();

    //Point light
    for (uint i = 0; i < point_count; i++)
    {
        // First create shadow maps And Render meshs to shadow texture
        point_light_vec_[i]->RenderShadowMap();
    }

    // Direc light
    for (uint i = 0; i < direc_count; i++)
    {
        // First create shadow maps And Render meshs to shadow texture
        direc_light_vec_[i]->RenderShadowMap();
    }
}
