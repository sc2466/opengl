#include "DepthFB.h"

DepthFB::DepthFB(uint width, uint height)
: FrameBuffer(width, height)
{
    depth_tex_ = new DepthTexture(width, height);
}

DepthFB::~DepthFB()
{
    delete depth_tex_;
}


void DepthFB::Create()
{
    // Create Framebuffer
    FrameBuffer::Create();

    // Create floating point color buffer
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_);

    depth_tex_->Create();

    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, *depth_tex_, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;

    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void DepthFB::Render() const
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, *texArray_);
    QuadMesh::get()->Render();
}
