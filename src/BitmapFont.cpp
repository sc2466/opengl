#include "BitmapFont.h"
#include <fstream>

BitmapFont::BitmapFont(const char* path)
{
    fontTex_.LoadImageA(path);
}

BitmapFont::~BitmapFont()
{
    //dtor
}


bool BitmapFont::parseFont(const String& path, Charset& CharsetDesc)
{
    String Line;
    String Read, Key, Value;
    std::size_t i;
    std::ifstream fntFile(path);
    std::istream& Stream = fntFile;

    if(!fntFile)
        ErrorPrint("FILE_NOT_SUCCESFULLY_READ : " << path);

    while( !Stream.eof() )
    {
        std::stringstream LineStream;
        std::getline( Stream, Line );
        LineStream << Line;

        //read the line's type
        LineStream >> Read;
        if( Read == "common" )
        {
            //this holds common data
            while( !LineStream.eof() )
            {
                std::stringstream Converter;
                LineStream >> Read;
                i = Read.find( '=' );
                Key = Read.substr( 0, i );
                Value = Read.substr( i + 1 );

                //assign the correct value
                Converter << Value;
                if( Key == "lineHeight" )
                    Converter >> CharsetDesc.LineHeight;
                else if( Key == "base" )
                    Converter >> CharsetDesc.Base;
                else if( Key == "scaleW" )
                    Converter >> CharsetDesc.Width;
                else if( Key == "scaleH" )
                    Converter >> CharsetDesc.Height;
                else if( Key == "pages" )
                    Converter >> CharsetDesc.Pages;
            }
        }
        else if( Read == "char" )
        {
            //this is data for a specific char
            unsigned short CharID = 0;

            while( !LineStream.eof() )
            {
                std::stringstream Converter;
                LineStream >> Read;
                i = Read.find( '=' );
                Key = Read.substr( 0, i );
                Value = Read.substr( i + 1 );

                //assign the correct value
                Converter << Value;
                if( Key == "id" )
                    Converter >> CharID;
                else if( Key == "x" )
                    Converter >> CharsetDesc.Chars[CharID].x;
                else if( Key == "y" )
                    Converter >> CharsetDesc.Chars[CharID].y;
                else if( Key == "width" )
                    Converter >> CharsetDesc.Chars[CharID].Width;
                else if( Key == "height" )
                    Converter >> CharsetDesc.Chars[CharID].Height;
                else if( Key == "xoffset" )
                    Converter >> CharsetDesc.Chars[CharID].XOffset;
                else if( Key == "yoffset" )
                    Converter >> CharsetDesc.Chars[CharID].YOffset;
                else if( Key == "xadvance" )
                    Converter >> CharsetDesc.Chars[CharID].XAdvance;
                else if( Key == "page" )
                    Converter >> CharsetDesc.Chars[CharID].Page;
            }
        }
    }

    fntFile.close();
    return true;
}

void BitmapFont::render(const String& text, const Vec2& pos, GLfloat scale, const Vec3& color)
{
    ushort CharX, CharY;
    ushort Width, Height;
    ushort OffsetX, OffsetY;
    ushort CurX;
    uint tSize = text.size();
    Vertex* Verts = new Vertex[tSize*4];

    useShader();
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE0, fontTex_);

    //TODO need to add line offset

    for( unsigned int i = 0; i < tSize; ++i )
    {
        CharX = m_Charset.Chars[text[i]].x;
        CharY = m_Charset.Chars[text[i]].y;
        Width = m_Charset.Chars[text[i]].Width;
        Height = m_Charset.Chars[text[i]].Height;
        OffsetX = m_Charset.Chars[text[i]].XOffset;
        OffsetY = m_Charset.Chars[text[i]].YOffset;

        //upper left
        Verts[i*4].tu = (float) CharX / (float) m_Charset.Width;
        Verts[i*4].tv = (float) CharY / (float) m_Charset.Height;
        Verts[i*4].x = (float)CurX + OffsetX;
        Verts[i*4].y = (float)OffsetY;

        //upper right
        Verts[i*4+1].tu = (float) (CharX+Width) / (float) m_Charset.Width;
        Verts[i*4+1].tv = (float) CharY / (float) m_Charset.Height;
        Verts[i*4+1].x = (float)Width + CurX + OffsetX;
        Verts[i*4+1].y = (float)OffsetY;

        //lower right
        Verts[i*4+2].tu = (float) (CharX+Width) / (float) m_Charset.Width;
        Verts[i*4+2].tv = (float) (CharY+Height) / (float) m_Charset.Height;
        Verts[i*4+2].x = (float)Width + CurX + OffsetX;
        Verts[i*4+2].y = (float)Height + OffsetY;

        //lower left
        Verts[i*4+3].tu = (float) CharX / (float) m_Charset.Width;
        Verts[i*4+3].tv = (float) (CharY+Height) / (float) m_Charset.Height;
        Verts[i*4+3].x = (float)CurX + OffsetX;
        Verts[i*4+3].y = (float)Height + OffsetY;

        CurX += m_Charset.Chars[text[i]].XAdvance;
    }

    glBindBuffer(GL_ARRAY_BUFFER, vbo_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(GLfloat) * (tSize*4) * 4, NULL, GL_DYNAMIC_DRAW);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), 0);

    delete[] Verts;
}

void BitmapFont::init()
{
    Font::init();
}
