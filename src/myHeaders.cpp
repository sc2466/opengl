#include "myHeaders.h"
#include <sstream>

const String intToString(int n)
{
    std::stringstream s;
    s << n;
    return s.str();
}
const String floatToString(float n)
{
    std::stringstream s;
    s << n;
    return s.str();
}

const String getArrayChar(const String& name, const unsigned i, const String& propertyName)
{
    std::ostringstream ss;
    ss << name << "[" << i << "]." << propertyName;
    return ss.str();
}
