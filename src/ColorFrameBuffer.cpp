#include "ColorFrameBuffer.h"

ColorFrameBuffer::ColorFrameBuffer(uint width, uint height)
: FrameBuffer(width, height)
{
    Create();
}

ColorFrameBuffer::~ColorFrameBuffer()
{
}



void ColorFrameBuffer::Create()
{
    // Create Framebuffer And set width, height value
    FrameBuffer::Create();

    // create floating point color buffer
    glBindFramebuffer(GL_FRAMEBUFFER, fbo_);

    // Create texture
    texArray_ = new uint;
    glGenTextures(1, texArray_);
    texCount_ = 1;

    // color : texArray_[0]
    glBindTexture(GL_TEXTURE_2D, *texArray_);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16F, width_, height_, 0, GL_RGB, GL_FLOAT, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, *texArray_, 0);

    if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
        std::cout << "Framebuffer not complete!" << std::endl;
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
}

void ColorFrameBuffer::Render() const
{
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, *texArray_);
    QuadMesh::get()->Render();
}
