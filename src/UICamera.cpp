#include "UICamera.h"

UICamera::UICamera(const GameWindow* window)
:width_(window->width_), height_(window->height_)
{}


Mat4& UICamera::getMat4(){
    orthProj_ = glm::ortho((uint)0, width_, (uint)0, height_);
}
