#include "FPSCamera.h"
#include "GameWindow.h"
#include "Scene.h"

FPSCamera::FPSCamera(Camera* camera)
: camera_(camera)
{
    //ctor
    const GameWindow* window = GameWindow::get();
    GLFWwindow* glfwWin = (GLFWwindow*)window->getGLFWWindow();
    glfwSetCursorPos(glfwWin, window->getHalfWidth(), window->getHalfHeight());
}

void FPSCamera::update()
{
    const GameWindow* window = GameWindow::get();
    GLFWwindow* glfwWin = (GLFWwindow*)window->getGLFWWindow();

    float deltaTime = window->getScene()->getDeltaTime();

    double prevX = (double)window->getHalfWidth();
    double prevY = (double)window->getHalfHeight();
    double offX, offY;

    glfwGetCursorPos(glfwWin, &offX, &offY);
    offX = offX-prevX;
    offY = prevY-offY;

    glfwSetCursorPos(glfwWin, prevX, prevY);

    camera_->setPitch(camera_->getPitch()+offY*mouseSpeed);
    camera_->setYaw(camera_->getYaw()+offX*mouseSpeed);

    // Move camera here

    if (glfwGetKey(glfwWin, GLFW_KEY_W) == GLFW_PRESS)
        camera_->setPos(camera_->getPos()+camera_->getForward()*moveSpeed*deltaTime);
    if (glfwGetKey(glfwWin, GLFW_KEY_S) == GLFW_PRESS)
        camera_->setPos(camera_->getPos()+camera_->getForward()*-moveSpeed*deltaTime);
    if (glfwGetKey(glfwWin, GLFW_KEY_A) == GLFW_PRESS)
        camera_->setPos(camera_->getPos()+camera_->getRight()*-moveSpeed*deltaTime);
    if (glfwGetKey(glfwWin, GLFW_KEY_D) == GLFW_PRESS)
        camera_->setPos(camera_->getPos()+camera_->getRight()*moveSpeed*deltaTime);

    camera_->calculate();
}
