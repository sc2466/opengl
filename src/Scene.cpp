#include "Scene.h"
#include "GameWindow.h"
#include "QuadMesh.h"
#include "SubShader.h"
#include "FtFont.h"
#include <sstream>

using namespace std;

Scene::Scene(GameWindow* window)
: window_(window),
 width_(window->getWidth()), height_(window->getHeight())
{
    init();
}

void Scene::init()
{
    glEnable(GL_DEPTH_TEST);
    //glEnable(GL_BLEND);
    //glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    ft = new FtFont();
    fCamera = new FPSCamera(&camera_);

    const char* unifomrNames[] = {
    "TransformBlock.scale",
    "TransformBlock.translation",
    "TransformBlock.color",
    "TransformBlock.proj",
    "TransformBlock.mv"
    }
    GLuint uniformIndices[5];
    Shader test_shader("shader/Ttest.vs","shader/Ttest.fs");
    glGetUniformIndices(test_shader.get(), 5, unifomrNames, uniformIndices)

    GLint uniformOffsets[5];
    GLint arrayStrides[5];
    GLint matrixStrides[5];
    glGetActiveUniformsiv(test_shader, 5, uniformIndices, GL_UNIFORM_OFFSET, uniformOffsets);
    glGetActiveUniformsiv(test_shader, 5, uniformIndices, GL_UNIFORM_ARRAY_STRIDE, arrayStrides);
    glGetActiveUniformsiv(test_shader, 5, uniformIndices, GL_UNIFORM_MATRIX_STRIDE, matrixStrides);

    for(auto t : uniformIndices){
        DebugPrint("")
    }


    Mesh* quadMesh = QuadMesh::get();
    Shader* spriteShader = SubShader::getSpriteShader();
    meshRendererManager.create(quadMesh, spriteShader);
}


void Scene::loop()
{

    // Render Loop
    GLFWwindow* glfw_window = window_->getGLFWWindow();

    Mesh* quadMesh = QuadMesh::get();
    Shader* spriteShader = SubShader::getSpriteShader();

    float checkDelayTime = 0, finalTime = 0;

    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
    int x=0,y=0;

    while(!glfwWindowShouldClose(glfw_window))
    {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        //glfwSetCursorPos(glfw_window)
        x += InputSystem::get()->getMouseXOffset();
        y += InputSystem::get()->getMouseYOffset();
        //Vec3(x*-.01f,y*-.01f,10);
        //camera_.setPos();
        //camera_.setPitch(y*-.01f);
        //camera_.setYaw(-90.f+x*-.01f);
        fCamera->update();


        spriteShader->useProgram();
        spriteShader->setVec3("color", Vec3(0,1,0));
        meshRendererManager.render(&camera_);

        Mat4 a = Mat4();
        a = glm::translate(a, Vec3(1,0,0));
        a = glm::scale(a, Vec3(.5f));

        spriteShader->setMat4("mvp", a);
        spriteShader->setVec3("color", Vec3(1,0,0));
        quadMesh->Render();

        // render text
        std::ostringstream o;
        o << "FPS " << delay_fps_;
        glDisable(GL_DEPTH_TEST);
        ft->renderText(o.str());
        glEnable(GL_DEPTH_TEST);

        // Calculate DeltaTime
        float currentTime = glfwGetTime();
        deltaTime_ = currentTime - finalTime;
        finalTime = currentTime;
        fps_ = 1/deltaTime_;

        // Calculate 0.1 sec delay DeltaTime
        checkDelayTime += deltaTime_;
        if(checkDelayTime > .1f){
            checkDelayTime = 0;
            delay_fps_ = fps_;
        }

        // Poll Events And
        // Swap buffer for Render
        glfwSwapBuffers(glfw_window);
        glfwPollEvents();

        // For get input keys
        InputSystem::get()->processInput(window_);
    }
}


Scene* Scene::self_ = nullptr;
