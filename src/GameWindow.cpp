#include "GameWindow.h"


#include "EDebug.h"
#include "Scene.h"
#include "InputSystem.h"


GameWindow::GameWindow(uint width, uint height)
: width_(width), height_(height)
{
    self_ = this;
}

GameWindow::~GameWindow()
{
    if(glfw_window_ != nullptr){
    glfwDestroyWindow(glfw_window_);
    glfwTerminate();
    }
    glfw_window_ = nullptr;
}

int GameWindow::GLAD_Init()
{
    // glad: load all OpenGL function pointers
    // ---------------------------------------
    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        ErrorPrint("Failed to initialize GLAD");
        return -1;
    }
    return 0;
}

void error_callback(int error, const char* description);

int GameWindow::GLFW_Init()
{
    /*
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    return 0;
    */

    if(!glfwInit()) return -1;
    glfwSetErrorCallback(error_callback);

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);

#ifdef SIBALSSAJIVANG
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
    glfwWindowHint(GLFW_OPENGL_ANY_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#else
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
#endif

#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // uncomment this statement to fix compilation on OS X
#endif
    return 0;
}

int GameWindow::create()
{
    if(GLFW_Init()) return -1;
    //glfwWindowHint(GLFW_SAMPLES, 4);

    glfw_window_ = glfwCreateWindow(width_, height_, name_.c_str(), NULL, NULL);
    if (glfw_window_ == NULL)
    {
        ErrorPrint("Fail to create Window!");
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(glfw_window_);
    glfwSwapInterval(1);

    glfwSetFramebufferSizeCallback(glfw_window_, GameWindow::framebuffer_size_callback);
    glfwSetCursorPosCallback(glfw_window_, InputSystem::mouseCallback);
    glfwSetMouseButtonCallback(glfw_window_, InputSystem::mouseButtonCallback);
    glfwSetKeyCallback(glfw_window_, InputSystem::keyCallback);

    if(GLAD_Init()) return -1;
    EDebug::PrintOpenglState();

    scene_ = new Scene(this);
    GameWindow::framebuffer_size_callback(glfw_window_, width_, height_);
    return 0;
}


void GameWindow::loop()
{
    scene_->loop();
    //std::thread th(&Scene::loop, scene_);
    //while(!glfwWindowShouldClose(glfw_window_))
    //    glfwPollEvents();
    //th.join();
}

void GameWindow::setCursorEnable(uint bIsEnable)
{
    if(bIsEnable)
        //커서 보이게 하기
        glfwSetInputMode(glfw_window_, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
    else
        //커서 안보이게 하기
        glfwSetInputMode(glfw_window_, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
}

void GameWindow::framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    // make sure the viewport matches the new window dimensions; note that width and
    // height will be significantly larger than specified on retina displays.
    glViewport(0, 0, width, height);

    GameWindow* win = GameWindow::get();
    win->width_ = width;
    win->height_ = height;
    win->halfWidth_ = width >> 1;
    win->halfHeight_ = height >> 1;

    win->scene_->getMainCamera().setSize(width, height);
}

GameWindow* GameWindow::self_ = nullptr;

void error_callback(int error, const char* description)
{
    ErrorPrint("Error(glfw): "<<description<<"\n");
}
