#include "QuadMesh.h"

QuadMesh* QuadMesh::quadMesh_ = nullptr;
uint QuadMesh::vao_ = 0;
uint QuadMesh::vbo_ = 0;

QuadMesh::QuadMesh()
{
    if (QuadMesh::vao_ == 0)
    {
        float quadVertices[] = {
            // positions        // texture Coords
            -1.0f,  1.0f, 0.0f, 1.0f,
            -1.0f, -1.0f, 0.0f, 0.0f,
             1.0f,  1.0f, 1.0f, 1.0f,
             1.0f, -1.0f, 1.0f, 0.0f,
        };
        // setup plane quadMesh_::vao_
        glGenVertexArrays(1, &QuadMesh::vao_);
        glGenBuffers(1, &QuadMesh::vbo_);
        glBindVertexArray(QuadMesh::vao_);
        glBindBuffer(GL_ARRAY_BUFFER, QuadMesh::vbo_);
        glBufferData(GL_ARRAY_BUFFER, sizeof(quadVertices), &quadVertices, GL_STATIC_DRAW);
        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(float), (void*)0);
    }
}

QuadMesh::~QuadMesh()
{
    //dtor
}

// renderQuad() renders a 1x1 XY quad in NDC
// -----------------------------------------
void QuadMesh::Render()
{
    glBindVertexArray(QuadMesh::vao_);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    glBindVertexArray(0);
}
