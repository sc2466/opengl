#include "Texture.h"

Texture::~Texture()
{
    glDeleteTextures(1, &textureID_);
}

void Texture::LoadImage(const char* resourece, bool gammaCorrection)
{
	glGenTextures(1, &textureID_);
    glBindTexture(GL_TEXTURE_2D, textureID_);

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	unsigned char *data = stbi_load(resourece, &width_, &height_, &nrComponents_, 0);
	if(data)
	{
	    GLenum internalFormat;
		GLenum format;
		if (nrComponents_ == 1)
            format = GL_RED;
        else if (nrComponents_ == 3){
            internalFormat = gammaCorrection ? GL_SRGB : GL_RGB;
            format = GL_RGB;
        }else if (nrComponents_ == 4){
            internalFormat = gammaCorrection ? GL_SRGB_ALPHA : GL_RGBA;
            format = GL_RGBA;
            glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width_, height_, 0, format, GL_UNSIGNED_BYTE, data);
            glGenerateMipmap(GL_TEXTURE_2D);
        }
    }else{
        std::cout << "Failed to load texture : " <<resourece<< std::endl;
    }
    stbi_image_free(data);
}

uint Texture::LoadCubemap(Vector<const char*> faces)
{
    glGenTextures(1, &textureID_);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID_);
    unsigned char *data = 0;
    int width, height, nrComponents;
    for (uint i = 0; i < faces.size(); i++)
    {
        //std::cout << faces[i] << std::endl;
        data = stbi_load(faces[i], &width, &height, &nrComponents, 0);
        if (data)
        {
            GLenum format;
            if (nrComponents == 1)
                format = GL_RED;
            else if (nrComponents == 3)
                format = GL_RGB;
            else if (nrComponents == 4)
                format = GL_RGBA;
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                         0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data
            );
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
        }
    }
    stbi_image_free(data);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID_;
}
