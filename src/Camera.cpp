#include "Camera.h"
#include "GameWindow.h"

const float Camera::YAW        = -90.0f;
const float Camera::PITCH      =  0.0f;
const float Camera::ZOOM       =  45.0f;
const float Camera::FOV        =  45.0f;

Camera::Camera(const Vec3& position, const Vec3& up, float yaw, float pitch)
: zoom_(ZOOM), forward_(Vec3(0,0,-1)),
fov_(FOV)
{
    width_ = GameWindow::get()->getWidth();
    height_ = GameWindow::get()->getHeight();
    position_ = position;
    world_up_ = up;
    yaw_ = yaw;
    pitch_ = pitch;
    calculate();
}

Camera::Camera(float posX, float posY, float posZ, float upX, float upY, float upZ, float yaw, float pitch)
: forward_(Vec3(0.0f, 0.0f, -1.0f)), zoom_(ZOOM)
{
    position_ = Vec3(posX, posY, posZ);
    world_up_ = Vec3(upX, upY, upZ);
    yaw_ = yaw;
    pitch_ = pitch;
    calculate();
}
/*
 void Camera::ProcessKeyboard(Camera_Movement direction, float deltaTime)
{
    float velocity = movement_speed_ * deltaTime;
    if (direction == FORWARD)
        position_ += front_ * velocity;
    if (direction == BACKWARD)
        position_ -= front_ * velocity;
    if (direction == LEFT)
        position_ -= right_ * velocity;
    if (direction == RIGHT)
        position_ += right_ * velocity;

    bIsNeedCal_ = true;
}


void Camera::ProcessMouseMovement(float xoffset, float yoffset, GLboolean constrainPitch)
{
    yaw_   += xoffset;
    pitch_ += yoffset;

    // Make sure that when pitch is out of bounds, screen doesn't get flipped
    if (constrainPitch)
    {
        if (pitch_ > 89.0f)
            pitch_ = 89.0f;
        if (pitch_ < -89.0f)
            pitch_ = -89.0f;
    }
    bIsNeedCal_ = true;
}
*/
void Camera::calculate(){
    //if no need update just return! For optimize
    if(bIsNeedCalView_){
        bIsNeedCalView_ = 0;

        // Calculate the new front_ Vector
        Vec3 front;
        front.x = cos(glm::radians(yaw_)) * cos(glm::radians(pitch_));
        front.y = sin(glm::radians(pitch_));
        front.z = sin(glm::radians(yaw_)) * cos(glm::radians(pitch_));
        front = glm::normalize(front);

        // Also re-calculate the right_ and up_ Vector
        // Normalize the vectors, because their length gets closer to 0 the more you look up
        // or down which results in slower movement.
        right_ = glm::normalize(glm::cross(front, world_up_));
        up_    = glm::normalize(glm::cross(right_, front));

        view_mat_ = glm::lookAt(position_, position_ + front, up_);
        forward_ = front;
    }
    if(bIsNeedCalProj_){
        bIsNeedCalProj_ = 0;
        proj_mat_ = glm::perspective(fov_, windowRatio_, near_, far_);
    }
    vpMat_ = proj_mat_ * view_mat_;
}
