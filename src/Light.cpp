#include "Light.h"
#include "LightManager.h"
#include "Scene.h"

Light::Light(const LightType type)
: position_(0), color_(1), type_(type)
{
    LightManager::get()->AddLight(this);
}

Light::Light(const LightType type, const Vec4& pos, const Vec3& color)
: position_(pos), color_(color), type_(type)
{
    LightManager::get()->AddLight(this);
}

Light::~Light()
{
    // Remove light form manager
    LightManager::get()->RemoveLight(light_id_, type_);
}

void Light::setPosition(const Vec3& pos)
{
    position_ = pos;
}

void Light::setColor(const Vec3& color)
{
    color_ = color;
}

Vec3& Light::getPosition() const
{
    return position_;
}

Vec3& Light::getColor() const
{
    return color_;
}

const LightType& getType() const
{
    return type_;
}
