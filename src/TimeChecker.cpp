#include "../include/TimeChecker.h"

TimeChecker::TimeChecker()
{
    //ctor
}

TimeChecker::~TimeChecker()
{
    //dtor
}

void TimeChecker::PrintDoubleNanoSec()
{
    std::cout << getDoubleNanoSec() << std::endl;
}

void TimeChecker::PrintNanoSec()
{
    std::cout << getNanoSec().count() << std::endl;
}

void TimeChecker::setStartTime()
{
    startPoint = std::chrono::system_clock::now();
}

void TimeChecker::setEndTime()
{
    endPoint = std::chrono::system_clock::now();
}

std::chrono::nanoseconds TimeChecker::getNanoSec()
{
    return endPoint - startPoint;
}

std::chrono::milliseconds TimeChecker::getMilliSec()
{
    return std::chrono::duration_cast<std::chrono::milliseconds>(endPoint - startPoint);
}

double TimeChecker::getDoubleNanoSec()
{
    std::chrono::duration<double> a = endPoint - startPoint;
    return a.count();
}
