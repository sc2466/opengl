#include "FrameBuffer.h"
#include "GameWindow.h"

FrameBuffer::FrameBuffer(uint width, uint height)
: width_(width), height_(height)
{Create();}

FrameBuffer::~FrameBuffer()
{
    Delete();
}

void FrameBuffer::Create()
{
#ifdef Debug
    cout << "FrameBuffer::Create()" << endl;
#endif
    if(fbo_ != 0 || texArray_ != nullptr)
        Delete();

    glGenFramebuffers(1, &fbo_);
}

void FrameBuffer::Delete()
{
    // Delete framebuffer
    if(fbo_ != 0)
        glDeleteFramebuffers(1, &fbo_);

    // Delete textures
    if(texArray_ != nullptr && texCount_ > 0)
        glDeleteTextures(texCount_, texArray_);
}

void FrameBuffer::CreateTextures(uint tex_count)
{
    if(tex_count == 0) return;
    texCount_ = tex_count;
    texArray_ = new uint[texCount_];
    glGenTextures(texCount_, texArray_);
}
