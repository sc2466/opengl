#include "PointLight.h"

PointLight::PointLight(const Vec3& pos, const Vec3& color) : Light(LightType.point, pos, color)
{
}

PointLight::~PointLight()
{
    //dtor
}
