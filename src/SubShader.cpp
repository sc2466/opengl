#include "SubShader.h"

Shader* SubShader::shadow_shader_ = nullptr;
Shader* SubShader::deferred_geo_shader_ = nullptr;
Shader* SubShader::sprite_shader_ = nullptr;
Shader* SubShader::deferred_shader_ = nullptr;
Shader*SubShader::textShader_ = nullptr;
