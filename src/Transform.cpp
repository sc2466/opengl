#include "Transform.h"


Transform::Transform(const glm::vec3& pos, const glm::vec3& rot, const glm::vec3& scale)
 : position_(pos), rotation_(rot), scale_(scale)
{

}

const Mat4& Transform::get()
{
    mat4_ = Mat4();
    mat4_ = glm::translate(mat4_, position_);
    if(rotation_.x != 0)
        mat4_ = glm::rotate(mat4_, glm::radians(rotation_.x), glm::vec3(1.0, 0.0, 0.0));
    if(rotation_.y != 0)
        mat4_ = glm::rotate(mat4_, glm::radians(rotation_.y), glm::vec3(0.0, 1.0, 0.0));
    if(rotation_.z != 0)
        mat4_ = glm::rotate(mat4_, glm::radians(rotation_.z), glm::vec3(0.0, 0.0, 1.0));
    mat4_ = glm::scale(mat4_, scale_);
    return mat4_;
}
