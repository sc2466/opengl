#include "MeshRenderer.h"

MeshRenderer::MeshRenderer(Mesh* mesh, Shader* shader)
: mesh_(mesh), shader_(shader)
{
    //ctor
}

MeshRenderer::~MeshRenderer()
{
    //dtor
}

void MeshRenderer::render(Camera* camera)
{
    if(shader_== nullptr || mesh_== nullptr) return;
    Shader* s = shader_;
    Camera* c = camera;
    s->use();
    uint model = s->getUniformID("model");
    uint mvp = s->getUniformID("mvp");
    s->setMat4(model, transform_.get());

    s->setMat4("view", c->getViewMatrix());
    s->setMat4("proj", c->getProjectionMatrix());
    s->setMat4("vp", c->getVpMatrix());

    s->setMat4(mvp, c->getVpMatrix()*transform_.get());
    mesh_->Render();
}
