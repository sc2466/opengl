#version 330 core
layout (location = 0) in vec3 aPos;

layout(std140) uniform TransformBlock{
    float scale;
    vec3 translation;
    float color[3];
    mat4 proj;
    mat4 mv;
} transform;

out vec3 _color;

void main()
{
    gl_Position = transform.proj * transform.mv * vec4(aPos * transform.scale, 1.0);
    _color = vec3(color[0], color[1], color[2]);
}
