#version 140 core

out vec4 _color;

int vec3 color;

void main()
{
    _color = color;
}
